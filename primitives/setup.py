import os
from setuptools import setup, find_packages

PACKAGE_NAME = 'test_primitives'


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    raise KeyError("'{0}' not found in '{1}'".format(key, module_path))


setup(
    name=PACKAGE_NAME,
    version=read_package_variable('__version__'),
    description='Test primitives',
    author=read_package_variable('__author__'),
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=[
        'd3m',
    ],
    url='https://gitlab.com/datadrivendiscovery/tests-data',
    keywords='d3m_primitive',
    entry_points = {
        'd3m.primitives': [
            'test.MonomialPrimitive = test_primitives.monomial:MonomialPrimitive',
            'test.IncrementPrimitive = test_primitives.increment:IncrementPrimitive',
            'test.SumPrimitive = test_primitives.sum:SumPrimitive',
            'test.RandomPrimitive = test_primitives.random:RandomPrimitive',
            'test.PrimitiveSumPrimitive = test_primitives.primitive_sum:PrimitiveSumPrimitive',
        ],
    },
)
